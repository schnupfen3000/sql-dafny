include "SQLTypeSystem.dfy"
include "SQLSemantics.dfy"
include "SoundnessAuxDefs.dfy"

module Progress
{
	import opened Environments
	import opened Tables
	import opened TableTypes
	import opened Opt
	import opened Queries
	import opened TypeSystem
	import opened QuerySemantics
	import opened StoreContextConsistency

	lemma successfulLookup(ttc: TTContext, ts: TStore, ref: string)
		requires StoreContextConsistent(ts, ttc)
		requires lookupEnv(ref, ttc).some?
		ensures lookupEnv(ref, ts).some?
	{
	}

	lemma welltypedLookup(ttc: TTContext, ts: TStore, ref: string)
		requires StoreContextConsistent(ts, ttc)
		requires lookupEnv(ref, ttc).some?
		requires lookupEnv(ref, ts).some?
		ensures welltypedtable(lookupEnv(ref, ttc).get, lookupEnv(ref, ts).get)
	{
	}

	lemma projectTypeAttrLImpliesfindAllColType(al: AttrL, tt: TType)
		requires projectTypeAttrL(al, tt).some?
		ensures  forall i :: 0 <= i < |al| ==> findColType(al[i], tt).some?;
	{}

  lemma dropFirstColRawPreservesWelltypedRaw(tt: TType, rt: RawTable)
    requires |tt| > 0
    requires welltypedRawtable(tt, rt)
    ensures welltypedRawtable(tt[1..], dropFirstColRaw(rt))
  {}

	lemma findColTypeImpliesfindCol(n: string, tt: TType, t: Table)
		requires welltypedtable(tt, t)
		requires findColType(n, tt).some?
		ensures findCol(n, t.getAL, t.getRaw).some?
	{
		if |t.getAL| == 0
		{}
		else
		{
			if n == t.getAL[0]
			{}
			else
			{
        dropFirstColRawPreservesWelltypedRaw(tt, t.getRaw);
				findColTypeImpliesfindCol(n, tt[1..], table(t.getAL[1..], dropFirstColRaw(t.getRaw)));
			}
		}
	}

	lemma projectTypeImpliesfindCol(al: AttrL, tt: TType, t: Table)
		requires welltypedtable(tt, t)
		requires projectTypeAttrL(al, tt).some?
		ensures forall i :: 0 <= i < |al| ==> findCol(al[i], t.getAL, t.getRaw).some?
	{
		if |al| == 0
		{}
		else
		{ 
			projectTypeAttrLImpliesfindAllColType(al, tt);
			forall n | 0 <= n < |al|
				ensures findCol(al[n], t.getAL, t.getRaw).some?
				{
					findColTypeImpliesfindCol(al[n], tt, t);
				}
		}
	}

	lemma projectColsProgress(al: AttrL, tt: TType, t: Table)
		requires welltypedtable(tt, t)
		requires projectType(list(al), tt).some?
		ensures projectCols(al, t.getAL, t.getRaw).some?
	{
		if |al| == 0
		{}
		else
		{projectTypeImpliesfindCol(al, tt, t);}
	}
	
	lemma projectTableProgress(s: Select, tt: TType, t: Table)
		requires welltypedtable(tt, t)
		requires projectType(s, tt).some?
		ensures projectTable(s, t).some?
	{
		match s
		{
			case all =>
			case list(al) => projectColsProgress(al, tt, t);
		}
	}

  lemma filterRowsPreservesTable(tt: TType, rt: RawTable, al: AttrL, p: Pred)
    requires welltypedRawtable(tt, rt)
    ensures welltypedRawtable(tt, filterRows(rt, al, p))
  {}

	lemma filterPreservesType(tt: TType, t: Table, p: Pred)
		requires welltypedtable(tt, t)
		ensures welltypedtable(tt, filterTable(t, p))
	{
    filterRowsPreservesTable(tt, t.getRaw, t.getAL, p);
	}

	lemma progress(ttc: TTContext, ts: TStore, q: Query)
		requires StoreContextConsistent(ts, ttc)
		requires exists tt :: typable(ttc, q, tt)
		ensures isValue(q) || reduce(q, ts).some?
	{
		match q
		{
			case tvalue(t) =>
			case selectFromWhere(sel, refs, p) =>
				if |refs| == 1
				{
				  successfulLookup(ttc, ts, refs[0]);
					welltypedLookup(ttc, ts, refs[0]);
					var t := lookupEnv(refs[0], ts).get;
					var tt := lookupEnv(refs[0], ttc).get;
					filterPreservesType(tt, t, p);
					projectTableProgress(sel, tt, filterTable(t, p));
				}
				else {}
			case union(q1, q2) =>
			case intersection(q1, q2) =>
			case difference(q1, q2) => 
		}
	}
	
}
