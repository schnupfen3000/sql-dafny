include "Util.dfy"

// untyped tables and basic functions to manipulate them
module Tables {
	import opened Opt
	// attributes and field values and types
	type AttrL = seq<string>
	datatype Val = N(int) | S(string) //allow int values and string values in table cells for now

	// how to specify default cases in Dafny?
	predicate greaterThan(v1: Val, v2: Val)
	{
		match (v1, v2)
		{
			case (N(n1), N(n2)) => n1 > n2
			case (S(s1), S(s2)) => |s1| > |s2| //preliminary specification!
			case (N(n1), S(s1)) => false
			case (S(s1), N(n1)) => false
		}
	}

	predicate lessThan(v1: Val, v2: Val)
	{
		match (v1, v2)
		{
			case (N(n1), N(n2)) => n1 < n2
			case (S(s1), S(s2)) => |s1| < |s2| //preliminary specification!
			case (N(n1), S(s1)) => false
			case (S(s1), N(n1)) => false
		}
	}

	// "raw" table without header	
	type Row = seq<Val>
	type RawTable = seq<Row>
		
	// full table with header (attribute list) 
	datatype Table = table(getAL: AttrL, getRaw: RawTable)

			// rowIn is just the infix "in" operator on sequences in Dafny ;)

	//projects a raw table to its first column
  //returns a raw table with exactly one column or tempty
	//TODO: is it a good idea to specify this as recursive function...?
	function projectFirstRaw(rt: RawTable): RawTable
	{
		if |rt| == 0
			then []
		else if rt[0] == []
			then [[]] + projectFirstRaw(rt[1..])
			else [[rt[0][0]]] + projectFirstRaw(rt[1..])
	}

	//drops the first column of a raw table
	//returns a raw table with one column less than before or tempty
	function dropFirstColRaw(rt: RawTable): RawTable
	{
		if |rt| == 0
			then []
		else if rt[0] == []
			then [[]] + dropFirstColRaw(rt[1..])
			else [rt[0][1..]] + dropFirstColRaw(rt[1..])
	}

	//attaches a raw table with one column to the front of another raw table
	//returns a raw table with one column more
	//blindly assumes that both tables have the same row count!
	//fails if input tables do not have the desired format
	//let-expressions would be nice for this specification...
	function attachColToFrontRaw(rt1: RawTable, rt2: RawTable): RawTable
	{
		if |rt1| == 0 && |rt2| == 0
			then []
		else if |rt1| > 0 && |rt1[0]| == 1 && |rt2| > 0
			then [[rt1[0][0]] + rt2[0]] + attachColToFrontRaw(rt1[1..], rt2[1..])
		else [[]] //ERROR CASE! idea: return a term which will cause the resulting table to be not welltyped...
			//should we model the explicitly (with option type)...? could this cause a soundness error later?
	}

	//blind row union - does not care about table types!
	//definition: union removes duplicate rows
	//(but only between the two tables, not within a table!)
	//preserves row order of the two original raw tables
	function rawUnion(rt1: RawTable, rt2: RawTable): RawTable
	{
		if |rt1| == 0
			then rt2
		else if |rt2| == 0
			then rt1
		else if rt1[0] in rt2
			then rawUnion(rt1[1..], rt2)
		else [rt1[0]] + rawUnion(rt1[1..], rt2)
	}

	//preserves order of rows in first argument
	//also ignores table types completely
	function rawIntersection(rt1: RawTable, rt2: RawTable): RawTable
	{
		if |rt1| == 0
			then []
		else if |rt2| == 0
			then []
		else if rt1[0] in rt2
			then [rt1[0]] + rawIntersection(rt1[1..], rt2)
		else rawIntersection(rt1[1..], rt2)
	}

	function rawDifference(rt1: RawTable, rt2: RawTable): RawTable
	{
		if |rt1| == 0
			then []
		else if |rt2| == 0
			then rt1
		else if rt1[0] in rt2
			then rawDifference(rt1[1..], rt2)
		else [rt1[0]] + rawDifference(rt1[1..], rt2)
	}

	//project one column out of a raw table, if the column can be found (fails otherwise)
	//TODO: explicitly model failure here or just return an empty table?
	function findCol(a: string, al: AttrL, rt: RawTable): Option<RawTable>
	{
		if |al| == 0
			then none
		else if a == al[0]
			then some(projectFirstRaw(rt))
		else findCol(a, al[1..], dropFirstColRaw(rt))
	}


	// project an empty column with as many rows as the given raw table
	function projectEmptyCol(rt: RawTable): RawTable
	{
		if |rt| == 0
		then []
		else [[]] + projectEmptyCol(rt[1..])
	}

	function projectCols(projectOn: AttrL, tableheader: AttrL, rt: RawTable): Option<RawTable>
	{
		if |projectOn| == 0
			then some(projectEmptyCol(rt))
		else if findCol(projectOn[0], tableheader, rt).some? && projectCols(projectOn[1..], tableheader, rt).some?
			then some(attachColToFrontRaw(findCol(projectOn[0], tableheader, rt).get, projectCols(projectOn[1..], tableheader, rt).get))
			else none
	}
}

// typed tables and definition of well-typedness of a table
module TableTypes {
	import opened Tables

	datatype FType = Num | Str //allow int and string as types for fields for now

	//assign types to field values (currently static)
	function fieldType(v: Val): FType
	{
		match v
		{
			case N(i) => Num
			case S(s) => Str
		}
	}
	
	//typed table schemas
	type TType = seq<(string, FType)>

	//welltypedness of tables
	predicate matchingAttrL(tt: TType, al: AttrL)
	{
		|tt| == |al| && forall i :: 0 <= i < |tt| ==> tt[i].0 == al[i] 
	}

	predicate welltypedRow(tt: TType, r: Row)
	{
		|tt| == |r| && forall i :: 0 <= i < |tt| ==> fieldType(r[i]) == tt[i].1
	}
	
	predicate welltypedRawtable(tt: TType, t: RawTable)
	{
		forall i :: 0 <= i < |t| ==> welltypedRow(tt, t[i])
	}

	predicate welltypedtable(tt: TType, t: Table)
	{
	  matchingAttrL(tt, t.getAL) && welltypedRawtable(tt, t.getRaw)	
	}
}

