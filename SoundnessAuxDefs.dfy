include "SQLSemantics.dfy"
include "SQLTypeSystem.dfy"

module StoreContextConsistency
{
	import opened TableTypes
	import opened TypeSystem
	import opened QuerySemantics

	predicate StoreContextConsistent(ts: TStore, ttc: TTContext)
	{
		(ts.empty? && ttc.empty?)
			||
			(ts.bind? && ttc.bind? && ts.name == ttc.name
			&& welltypedtable(ttc.elem, ts.elem) && StoreContextConsistent(ts.rest, ttc.rest))
	}
}
