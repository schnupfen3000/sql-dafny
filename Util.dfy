// datatypes and functions generally useful

// support for option datatype
module Opt {
	datatype Option<A> = none | some(get: A)
}

//generic environment datatype, e.g. for table stores and table type contexts
module Environments {
	import opened Opt
	//how to better modularize this?
  
  datatype Env<A> = empty | bind(name: string, elem: A, rest: Env<A>)

	function lookupEnv<A>(name: string, env: Env<A>): Option<A>
	{
		match env
		{
			case empty => none
			case bind(m, a, e) =>
				if name == m
				then some(a)
				else lookupEnv(name, e) 
		}
	}	
}

