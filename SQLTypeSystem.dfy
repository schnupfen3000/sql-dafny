include "SQLSyntax.dfy"
module TypeSystem {

	import opened Opt
	import opened Queries
	import opened Tables
	import opened TableTypes
	import opened Environments

	//table type contexts
	type TTContext = Env<TType>

	function findColType(n: string, tt: TType): Option<FType>
	{
		if |tt| == 0
		then none
		else if n == tt[0].0
			then some(tt[0].1)
			else findColType(n, tt[1..])
	}

	// project a table type on chosen attributes, if possible
	function projectTypeAttrL(al: AttrL, tt: TType): Option<TType>
	{
		if |al| == 0
		then some([])
		else if findColType(al[0], tt).some? && projectTypeAttrL(al[1..], tt).some?
			then some([(al[0], findColType(al[0], tt).get)] + projectTypeAttrL(al[1..], tt).get)
			else none
	}

	// lifting projectTypeAttrL to Select terms
	function projectType(sel: Select, tt: TType): Option<TType>
	{
		match sel
		{
			case all => some(tt)
			case list(al) => projectTypeAttrL(al, tt)
		}
	}

	// extract type of an expression from a predicate from a given table type
	function typeOfExp(e: Exp, tt: TType): Option<FType>
	{
		match e
		{
			case constant(fv) => some(fieldType(fv))
			case lookup(a) => if |tt| == 0
				then none
		    else if tt[0].0 == a
			    then some(tt[0].1)
		      else typeOfExp(e, tt[1..])
		}
	}

	// test whether a predicate is "compatible" with a given table type
	predicate tcheckPred(p: Pred, tt: TType)
	{
		match p
		{
			case ptrue => true
			case and(p1, p2) => tcheckPred(p1, tt) && tcheckPred(p2, tt)
			case not(p1) => tcheckPred(p1, tt)
			case eq(e1, e2) => typeOfExp(e1, tt).some? && typeOfExp(e2, tt).some?
				&& typeOfExp(e1, tt).get == typeOfExp(e2, tt).get
			case gt(e1, e2) => typeOfExp(e1, tt).some? && typeOfExp(e2, tt).some?
				&& typeOfExp(e1, tt).get == typeOfExp(e2, tt).get
			case lt(e1, e2) => typeOfExp(e1, tt).some? && typeOfExp(e2, tt).some?
				&& typeOfExp(e1, tt).get == typeOfExp(e2, tt).get
		}
	}

	// what is the best way to state a type system / inference rules in Dafny...?
	// predicate? function?

	predicate typable(ttc: TTContext, q: Query, tt: TType)
	{
		match q
		{
			case tvalue(t) => welltypedtable(tt, t)
			case selectFromWhere(sel, refs, p) => |refs| == 1 && lookupEnv(refs[0], ttc).some?
				&& tcheckPred(p, lookupEnv(refs[0], ttc).get) && projectType(sel, lookupEnv(refs[0], ttc).get) == some(tt)
			case union(q1, q2) => typable(ttc, q1, tt) && typable(ttc, q2, tt)
			case intersection(q1, q2) => typable(ttc, q1, tt) && typable(ttc, q2, tt)
			case difference(q1, q2) => typable(ttc, q1, tt) && typable(ttc, q2, tt)
		}
	}

}
