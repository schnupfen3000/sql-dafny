include "SQLSyntax.dfy"
module QuerySemantics {

	import opened Queries
	import opened Tables
	import opened Opt

	// projection on a table, using auxiliary function projectCols on raw table
	function projectTable(s: Select, t: Table): Option<Table>
	{
		match s
		{
			case all => some(t)
			case list(al) => if projectCols(al, t.getAL, t.getRaw).some?
				then some(table(al, projectCols(al, t.getAL, t.getRaw).get))
				else none
		}
	}

	//lookup table values if required in an expression -
	//fails if an attribute shall be looked up that does not exist in the table
	//or if the row is empty
	function evalExpRow(e: Exp, al: AttrL, r: Row): Option<Val>
	{
		match e
		{
			case constant(v) => some(v)
			case lookup(a) => if |al| > 0 && |r| > 0
				then if a == al[0]
				  then some(r[0])
				  else evalExpRow(e, al[1..], r[1..])
				else none
		}
	}

	// returns true iff predicate succeeds on row
	// returns false if predicate evaluates to false or if predicate evaluation fails
	predicate filterSingleRow(p: Pred, al: AttrL, r: Row)
	{
		match p
		{
			case ptrue => true
			case and(p1, p2) => filterSingleRow(p1, al, r) && filterSingleRow(p2, al, r)
			case not(p1) => !filterSingleRow(p1, al, r)
			case eq(e1, e2) => evalExpRow(e1, al, r).some? && evalExpRow(e2, al, r).some? && evalExpRow(e1, al, r).get == evalExpRow(e2, al, r).get
			case gt(e1, e2) => evalExpRow(e1, al, r).some? && evalExpRow(e2, al, r).some? &&
				greaterThan(evalExpRow(e1, al, r).get, evalExpRow(e2, al, r).get)
			case lt(e1, e2) => evalExpRow(e1, al, r).some? && evalExpRow(e2, al, r).some? &&
				lessThan(evalExpRow(e1, al, r).get, evalExpRow(e2, al, r).get)
		}
	}

	// filter rows that satisfy pred from raw table
  function filterRows(rt: RawTable, al: AttrL, p: Pred): RawTable
	{
		if |rt| == 0
			then []
		else if filterSingleRow(p, al, rt[0])
			then [rt[0]] + filterRows(rt[1..], al, p)
			else filterRows(rt[1..], al, p)
	}

	// filter rows that satisfy pred from full table
	function filterTable(t: Table, p: Pred): Table
	{
		table(t.getAL, filterRows(t.getRaw, t.getAL, p))
	}

	import opened Environments
	type TStore = Env<Table>

  // single reduction steps
	// reduce fails if referenced tables are not found
	// rather specify this using methods, to be able to use variables as containers of intermediate calculations?
	function reduce(q: Query, ts: TStore): Option<Query>
	{
		match q
		{
			case tvalue(t) => none
			case selectFromWhere(sel, ref, pred) =>
				if |ref| == 1 //currently, no Cartesian product supported, TODO!
				then if lookupEnv(ref[0], ts).some? && projectTable(sel, filterTable(lookupEnv(ref[0], ts).get, pred)).some?
				then some(tvalue(projectTable(sel, filterTable(lookupEnv(ref[0], ts).get, pred)).get))
				  else none
				else none 
			case union(q1, q2) => if q1.tvalue? then				
					if q2.tvalue?
					then some(tvalue(table(q1.getTable.getAL, rawUnion(q1.getTable.getRaw, q2.getTable.getRaw))))
					else if reduce(q2, ts).some? then some(union(q1, reduce(q2, ts).get)) else none
				else if reduce(q1, ts).some? then some(union(reduce(q1, ts).get, q2)) else none
			case intersection(q1, q2) => if q1.tvalue? then				
					if q2.tvalue?
					then some(tvalue(table(q1.getTable.getAL, rawIntersection(q1.getTable.getRaw, q2.getTable.getRaw))))
					else if reduce(q2, ts).some? then some(intersection(q1, reduce(q2, ts).get)) else none
				else if reduce(q1, ts).some? then some(intersection(reduce(q1, ts).get, q2)) else none
			case difference(q1, q2) => if q1.tvalue? then				
					if q2.tvalue?
					then some(tvalue(table(q1.getTable.getAL, rawDifference(q1.getTable.getRaw, q2.getTable.getRaw))))
					else if reduce(q2, ts).some? then some(difference(q1, reduce(q2, ts).get)) else none
				else if reduce(q1, ts).some? then some(difference(reduce(q1, ts).get, q2)) else none
		}
	}
	
}
