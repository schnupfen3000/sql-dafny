include "SQLTypeSystem.dfy"
include "SQLSemantics.dfy"
include "SoundnessAuxDefs.dfy"
include "ProgressProof.dfy"

module Preservation
{
  import opened Environments
  import opened Tables
  import opened TableTypes
  import opened Opt
  import opened Queries
  import opened TypeSystem
	import opened QuerySemantics
	import opened StoreContextConsistency
  import opened Progress

  lemma rawUnionPreservesWellTypedRaw(rt1: RawTable, rt2: RawTable, tt: TType)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawUnion(rt1, rt2))
  {}

  lemma rawIntersectionPreservesWellTypedRaw(rt1: RawTable, rt2: RawTable, tt: TType)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawIntersection(rt1, rt2))
  {}

  lemma rawDifferencePreservesWellTypedRaw(rt1: RawTable, rt2: RawTable, tt: TType)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawDifference(rt1, rt2))
  {}


  lemma projectTypeAttrLMatchesProjectTableAttrL(al: AttrL, tt: TType, t: Table)
		requires welltypedtable(tt, t)
		requires projectTypeAttrL(al, tt).some?
		requires projectTable(list(al), t).some?
		ensures matchingAttrL(projectTypeAttrL(al, tt).get, projectTable(list(al), t).get.getAL)
	{}

	lemma welltypedEmptyProjection(rt: RawTable)
		ensures welltypedRawtable([], projectEmptyCol(rt))
	{}

	lemma projectFirstRawPreservesWelltypedRaw(a: string, t: Table, tt: TType)
		requires welltypedRawtable(tt, t.getRaw)
		requires projectTypeAttrL([a], tt).some?
		requires a == tt[0].0
		ensures welltypedRawtable(projectTypeAttrL([a], tt).get, projectFirstRaw(t.getRaw))
		decreases |t.getRaw|
	{
		if |t.getRaw| == 0
		{}
		else
		{
			if (t.getRaw)[0] == []
			{}
			else
			{
				projectFirstRawPreservesWelltypedRaw(a, table(t.getAL, (t.getRaw)[1..]), tt);
			}
		}
	}

	lemma projectTypeFindCol(a: string, t: Table, tt: TType)
		requires welltypedtable(tt, t)
		requires projectTypeAttrL([a], tt).some?
		requires findCol(a, t.getAL, t.getRaw).some?
		ensures welltypedRawtable(projectTypeAttrL([a], tt).get, findCol(a, t.getAL, t.getRaw).get)
		decreases |t.getAL|
	{
		if |t.getAL| == 0
		{} //case not possible
		else
		{
			if a == t.getAL[0]
			{
				projectFirstRawPreservesWelltypedRaw(a, t, tt);
			}
			else
			{
				dropFirstColRawPreservesWelltypedRaw(tt, t.getRaw);
				projectTypeFindCol(a, table(t.getAL[1..], dropFirstColRaw(t.getRaw)), tt[1..]);
			}
		}
	}

	lemma attachColToFrontRawPreservesWelltypedRaw(tt1: TType, tt2: TType, rt1: RawTable, rt2: RawTable)
		requires |tt1| == 1
		requires |rt1| == |rt2| 
		requires welltypedRawtable(tt1, rt1)
		requires welltypedRawtable(tt2, rt2)
		ensures welltypedRawtable(tt1 + tt2, attachColToFrontRaw(rt1, rt2))
		ensures |rt1| == |attachColToFrontRaw(rt1, rt2)|
	{}

	lemma attachColToFrontRawPreservesRowCount(rt1: RawTable, rt2: RawTable)
		requires |rt1| == |rt2|
		requires forall i :: 0 <= i < |rt1| ==> |rt1[i]| == 1
		ensures |rt1| == |attachColToFrontRaw(rt1, rt2)|
	{}

	lemma projectFirstRawPreservesRowCount(rt: RawTable)
		ensures |rt| == |projectFirstRaw(rt)|
	{}

	lemma dropFirstColRawPreservesRowCount(rt: RawTable)
		ensures |rt| == |dropFirstColRaw(rt)|
	{}

	lemma findColPreservesRowCount(a: string, al: AttrL, rt: RawTable)
		requires findCol(a, al, rt).some?
		ensures |rt| == |findCol(a, al, rt).get|
	{
		if |al| == 0
		{}
		else
		{
			if a == al[0]
			{projectFirstRawPreservesRowCount(rt);}
			else
			{dropFirstColRawPreservesRowCount(rt);}
		}
	}

	lemma projectFirstRawYieldsSingleColumn(rt: RawTable)
		requires forall i :: 0 <= i < |rt| ==> |rt[i]| > 0
		ensures forall i :: 0 <= i < |projectFirstRaw(rt)| ==> |projectFirstRaw(rt)[i]| == 1
	{}

	lemma dropFirstColRawDecrementsColCount(rt: RawTable)
		requires |rt| == |dropFirstColRaw(rt)|
		requires forall i :: 0 <= i < |rt| ==> |rt[i]| > 0
		ensures forall i :: 0 <= i < |rt| ==> |rt[i]| == |dropFirstColRaw(rt)[i]| + 1
	{}

	lemma findColYieldsSingleColumn(a: string, al: AttrL, rt: RawTable)
		requires findCol(a, al, rt).some?
		requires forall i :: 0 <= i < |rt| ==> |al| == |rt[i]|
		ensures forall i :: 0 <= i < |findCol(a, al, rt).get| ==> |findCol(a, al, rt).get[i]| == 1
	{
		if |al| == 0
		{}
		else
		{
			if a == al[0]
			{
				assert forall i :: 0 <= i < |rt| ==> |rt[i]| > 0;
				projectFirstRawYieldsSingleColumn(rt);
			}
			else
			{
				dropFirstColRawPreservesRowCount(rt);
				dropFirstColRawDecrementsColCount(rt);
			}
		}
	}

	lemma projectEmptyColPreservesRowCount(rt: RawTable)
		ensures |rt| == |projectEmptyCol(rt)|
	{}

	lemma projectColsPreservesRowCount(al1: AttrL, al2: AttrL, rt: RawTable)
		requires projectCols(al1, al2, rt).some?
		requires forall i :: 0 <= i < |rt| ==> |al2| == |rt[i]|
		ensures |rt| == |projectCols(al1, al2, rt).get|
	{
		if |al1| == 0
		{projectEmptyColPreservesRowCount(rt);}
		else
		{
			if findCol(al1[0], al2, rt).some?
			{
				findColPreservesRowCount(al1[0], al2, rt);				
				findColYieldsSingleColumn(al1[0], al2, rt);
				projectColsPreservesRowCount(al1[1..], al2, rt);
				attachColToFrontRawPreservesRowCount(findCol(al1[0], al2, rt).get, projectCols(al1[1..], al2, rt).get);
			}
			else
			{}
		}
	}
	
	lemma projectColsWelltypedWithSelectType(al: AttrL, t: Table, tt: TType)
		requires welltypedtable(tt, t)
		requires projectTypeAttrL(al, tt).some?
		requires projectCols(al, t.getAL, t.getRaw).some?
		ensures welltypedRawtable(projectTypeAttrL(al, tt).get, projectCols(al, t.getAL, t.getRaw).get)
	{
		if |al| == 0
		{ welltypedEmptyProjection(t.getRaw); }
		else
		{
			projectTypeFindCol(al[0], t, tt);
			findColPreservesRowCount(al[0], t.getAL, t.getRaw);
			projectColsPreservesRowCount(al[1..], t.getAL, t.getRaw);
			attachColToFrontRawPreservesWelltypedRaw(projectTypeAttrL([al[0]], tt).get, projectTypeAttrL(al[1..], tt).get,
			 findCol(al[0], t.getAL, t.getRaw).get, projectCols(al[1..], t.getAL, t.getRaw).get);
		}
	}
		

  lemma projectTableWelltypedWithSelectType(sel: Select, t: Table, tt: TType)
    requires welltypedtable(tt, t)
    requires projectType(sel, tt).some?
    ensures projectTable(sel, t).some?
    ensures welltypedtable(projectType(sel, tt).get, projectTable(sel, t).get)
  {
    match sel 
    {
      case all =>
      case list(al) => {
        projectTableProgress(sel, tt, t);
        projectTypeAttrLMatchesProjectTableAttrL(al, tt, t);
				projectColsWelltypedWithSelectType(al, t, tt);
      }
    }
  }

  lemma preservation(ttc: TTContext, ts: TStore, q: Query, q': Query, tt: TType)
    requires StoreContextConsistent(ts, ttc)
    requires typable(ttc, q, tt)
    requires reduce(q, ts) == some(q')
    ensures typable(ttc, q', tt)
  {
    match q
    {
      case tvalue(t) =>
      case selectFromWhere(sel, refs, p) =>
        if |refs| == 1
        {
          successfulLookup(ttc, ts, refs[0]);
					welltypedLookup(ttc, ts, refs[0]);
					var t := lookupEnv(refs[0], ts).get;
          var tt' := lookupEnv(refs[0], ttc).get;
					filterPreservesType(tt', t, p);
          projectTableWelltypedWithSelectType(sel, filterTable(t, p), tt');
        }
        else {}
      case union(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawUnionPreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}
      case intersection(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawIntersectionPreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}
      case difference(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawDifferencePreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}     
    }
  }


}
