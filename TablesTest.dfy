include "Tables.dfy"

module TableTest {
	import opened Tables
	
	method test1()
	{
		var rt0: RawTable := [[], []];
		var rt0res := [[], []];
		assert projectFirstRaw(rt0) == rt0res;
		var rt1 := [[N(1), N(2), N(3)], [N(4), N(5), N(6)], [N(7), N(8), N(9)]];
		var rt1res := [[N(1)], [N(4)], [N(7)]];
		assert projectFirstRaw(rt1) == rt1res;
	}

	method test2()
	{
		var rt0: RawTable := [[], []];
		var rt0res := [[], []];
		assert dropFirstColRaw(rt0) == rt0res;
		var rt1 := [[N(1), N(2), N(3)], [N(4), N(5), N(6)], [N(7), N(8), N(9)]];
		var rt1res := [[N(2), N(3)], [N(5), N(6)], [N(8), N(9)]];
		//assert rt1[0][1..] == rt1res[0];
		assert forall i :: 0 <= i < |rt1| ==> rt1[i][1..] == rt1res[i];
		assert dropFirstColRaw(rt1) == rt1res;
	}

	method test3()
	{
		var rt1 := [[N(1)], [N(2)], [N(3)]];
		assert forall i :: 0 <= i < |rt1| ==> |rt1[i]| == 1;
		var rt2 := [[N(2), N(3)], [N(4), N(5)], [N(6), N(7)]];
		var rtresult := [[N(1), N(2), N(3)], [N(2), N(4), N(5)], [N(3), N(6), N(7)]];
		assert forall i :: 0 <= i < |rt1| ==> [rt1[i][0]] + rt2[i] == rtresult[i];
		assert attachColToFrontRaw(rt1, rt2) == rtresult;
		//interesting: this works much better if attachColToFrontRaw does not return an option value
	}
}
