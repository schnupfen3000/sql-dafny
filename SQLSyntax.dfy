include "Tables.dfy"
module Queries
{
	import opened Tables

	datatype Select = all | list(getList: AttrL)

	type TRef = seq<string>

	datatype Exp = constant(Val) | lookup(string)

	datatype Pred = ptrue
		| and(Pred, Pred)
		| not(Pred)
		| eq(Exp, Exp)
		| gt(Exp, Exp)
		| lt(Exp, Exp)
		
	datatype Query = tvalue(getTable: Table)
		| selectFromWhere(Select, TRef, Pred)
		| union(Query, Query)
		| intersection(Query, Query)
		| difference(Query, Query)

	predicate isValue(q: Query)
	{
		q.tvalue?
	}	
}
